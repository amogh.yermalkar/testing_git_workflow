package main

import "fmt"

func main() {
	fmt.Println("main")
}

func featureOne() {
	fmt.Println("i am feature One")
}

func featureTwo() {
	fmt.Println("i am feature two")
}

func featureThree() {
	fmt.Println("i am feature Three")
}
